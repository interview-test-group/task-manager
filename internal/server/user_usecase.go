package server

import (
	"context"
	"gitlab.com/interview-test-group/task-manager/internal/config"
	"gitlab.com/interview-test-group/task-manager/internal/db"
)

type UserUsecase interface {
	Fetch(ctx context.Context) ([]User, error)
	GetByID(ctx context.Context, id string) (User, error)
	Update(ctx context.Context, ar *User) error
	Store(ctx context.Context, a *User) (string, error)
	Delete(ctx context.Context, id string) error
}

type userUsecase struct {
	conf     *config.Config
	userRepo db.UserRepository
}

func NewUserUsecase(conf *config.Config, userRepo db.UserRepository) UserUsecase {
	return &userUsecase{
		conf:     conf,
		userRepo: userRepo,
	}
}
func (h *userUsecase) Fetch(ctx context.Context) ([]User, error) {
	users, err := h.userRepo.Fetch(ctx)
	if err != nil {
		return nil, err
	}

	list := make([]User, len(users))
	for i, u := range users {
		list[i] = User{
			ID:        u.ID,
			Firstname: u.Firstname,
			Lastname:  u.Lastname,
		}
	}
	return list, nil
}

func (h *userUsecase) GetByID(ctx context.Context, id string) (User, error) {
	user, err := h.userRepo.GetByID(ctx, id)
	if err != nil {
		return User{}, err
	}

	data := User{
		ID:        user.ID,
		Firstname: user.Firstname,
		Lastname:  user.Lastname,
	}

	return data, nil
}

func (h *userUsecase) Store(ctx context.Context, u *User) (string,error) {
	user := &db.User{
		Firstname: u.Firstname,
		Lastname:  u.Lastname,
	}
	err := h.userRepo.Store(ctx, user)
	if err != nil {
		return "",err
	}
	return user.ID,nil
}

func (h *userUsecase) Update(ctx context.Context, user *User) error {
	_, err := h.userRepo.GetByID(ctx, user.ID)
	if err != nil {
		return err
	}
	u := &db.User{
		Model:     db.Model{ID: user.ID},
		Firstname: user.Firstname,
		Lastname:  user.Lastname,
	}

	err = h.userRepo.Update(ctx, u)
	if err != nil {
		return err
	}

	return nil
}

func (h *userUsecase) Delete(ctx context.Context, id string) error {
	err := h.userRepo.Delete(ctx, id)
	if err != nil {
		return err
	}
	return nil
}
