package server

import "time"

type User struct {
	ID        string `json:"id"`
	Firstname string `json:"first_name" validate:"required"`
	Lastname  string `json:"last_name" validate:"required"`
}

type Task struct {
	ID         string    `json:"id"`
	Title      string    `json:"title" validate:"required"`
	Content    string    `json:"content" validate:"required"`
	AssigneeID string    `json:"assignee_id" validate:"required"`
	Status     byte    `json:"status"`
	StartDate  time.Time `json:"start_date" validate:"required"`
	EndDate    time.Time `json:"end_date" validate:"required"`
	Reminder time.Time `json:"reminder" validate	:"required"`
}

type UpdateTaskStatusRequest struct {
	ID string
	Status    byte    `json:"status" validate:"required,oneof=0 1"`
}

//Error - custom error type
type Error struct {
	Code    string `json:"code"`
	Type    string `json:"type"`
	Message string `json:"message"`
}

func NewCustomBadRequestError(message string) *Error {
	return &Error{
		Code:    "400",
		Type:    "Bad Request",
		Message: message,
	}
}
func NewCustomInternalServerError(message string) *Error {
	return &Error{
		Code:    "500",
		Type:    "Server Error",
		Message: message,
	}
}
