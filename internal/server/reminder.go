package server

import (
	"fmt"
	"github.com/robfig/cron/v3"
	"gitlab.com/interview-test-group/task-manager/internal/notification"
	"log"
	"time"
)

type Reminder interface {
	AddReminder(date time.Time) (int, error)
	RemoveReminder(id int) error
}

type CronJob struct {
	cron *cron.Cron
	commander notification.Notifier
}

func NewReminder(notifier notification.Notifier) Reminder {
	return &CronJob{
		cron: cron.New(),
		commander: notifier,
	}
}

func (cj *CronJob) AddReminder(date time.Time) (int, error) {
	min := date.Minute()
	hour := date.Hour()
	_, month, day := date.Date()

	spec := fmt.Sprintf("%d %d %d %d *", min, hour, day, month)

	jid, err := cj.cron.AddFunc(spec, func() {
		log.Println("job run")
		cj.commander.Notify("remind")
	})

	if err != nil {
		return 0, err
	}

	cj.cron.Start()
	return int(jid), nil
}

func (cj *CronJob) RemoveReminder(id int) error {
	cj.cron.Remove(cron.EntryID(id))
	return nil
}
