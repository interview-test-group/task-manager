package server

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/interview-test-group/task-manager/internal/validator"
	"net/http"
	"strings"
)

type TaskHandler struct {
	validate *validator.Validator
	taskUsecase TaskUsecase
}

func NewTaskHandler(validate *validator.Validator, task TaskUsecase) *TaskHandler {
	return &TaskHandler{
		validate: validate,
		taskUsecase: task,
	}
}

func (h *TaskHandler) fetch(ctx echo.Context) error {
	tasks, err := h.taskUsecase.Fetch(ctx.Request().Context())
	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, NewCustomInternalServerError(err.Error()))
	}

	return ctx.JSON(http.StatusOK, tasks)
}

func (h *TaskHandler) getByID(ctx echo.Context) error {
	id := ctx.Param("id")
	task, err := h.taskUsecase.GetByID(ctx.Request().Context(), id)
	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, NewCustomInternalServerError(err.Error()))
	}

	return ctx.JSON(http.StatusOK, task)
}

func (h *TaskHandler) store(ctx echo.Context) error {
	task := Task{}
	if err := ctx.Bind(&task); err != nil {
		return ctx.JSON(http.StatusBadRequest, NewCustomBadRequestError(err.Error()))
	}

	err := h.validate.Validate.Struct(&task)
	if err != nil {
		errList := h.validate.TranslateError(err, h.validate.Trans)
		return ctx.JSON(http.StatusBadRequest, NewCustomBadRequestError(strings.Join(errList, ", ")))
	}

	id,err := h.taskUsecase.Store(ctx.Request().Context(), &task)
	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, NewCustomInternalServerError(err.Error()))
	}

	return ctx.JSON(http.StatusCreated, struct {
		ID string `json:"id"`
	}{
		ID: id,
	})
}

func (h *TaskHandler) updateStatus(ctx echo.Context) error {
	task := UpdateTaskStatusRequest{}
	id:=ctx.Param("id")
	if err := ctx.Bind(&task); err != nil {
		return ctx.JSON(http.StatusBadRequest, NewCustomBadRequestError(err.Error()))
	}

	err := h.validate.Validate.Struct(&task)
	if err != nil {
		errList := h.validate.TranslateError(err, h.validate.Trans)
		return ctx.JSON(http.StatusBadRequest, NewCustomBadRequestError(strings.Join(errList, ", ")))
	}
	task.ID=id
	err = h.taskUsecase.UpdateStatus(ctx.Request().Context(), &task)
	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, NewCustomInternalServerError(err.Error()))
	}

	return ctx.JSON(http.StatusOK, "task successfully updated")
}

func (h *TaskHandler) delete(ctx echo.Context) error {
	id := ctx.Param("id")
	err := h.taskUsecase.Delete(ctx.Request().Context(), id)
	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, NewCustomInternalServerError(err.Error()))
	}
	return ctx.JSON(http.StatusOK, "task successfully deleted")
}
