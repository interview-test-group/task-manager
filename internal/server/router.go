package server

import (
	"github.com/labstack/echo/v4"
)

// RegisterRoutes to the echo engine
func RegisterAllRoutes(e *echo.Echo, user *UserHandler, task *TaskHandler) {
	group := e.Group("")
	RegisterRoutes(group, user, task)
}
