package server

import (
	"context"
	"errors"
	"gitlab.com/interview-test-group/task-manager/internal/config"
	"gitlab.com/interview-test-group/task-manager/internal/db"
)

type TaskUsecase interface {
	Fetch(ctx context.Context) ([]Task, error)
	GetByID(ctx context.Context, id string) (Task, error)
	UpdateStatus(ctx context.Context, ar *UpdateTaskStatusRequest) error
	Store(ctx context.Context, a *Task) (string,error)
	Delete(ctx context.Context, id string) error
}

type taskUsecase struct {
	conf     *config.Config
	reminderRepo db.ReminderRepository
	taskRepo db.TaskRepository
	reminder Reminder
}

func NewTaskUsecase(conf *config.Config, taskRepo db.TaskRepository, reminder Reminder,	reminderRepo db.ReminderRepository) TaskUsecase {
	return &taskUsecase{
		conf:     conf,
		taskRepo: taskRepo,
		reminderRepo: reminderRepo,
		reminder: reminder,
	}
}
func (h *taskUsecase) Fetch(ctx context.Context) ([]Task, error) {
	tasks, err := h.taskRepo.Fetch(ctx)
	if err != nil {
		return nil, err
	}

	list := make([]Task, len(tasks))
	for i, t := range tasks {
		list[i] = Task{
			ID:         t.ID,
			Title:      t.Title,
			Content:    t.Content,
			AssigneeID: t.AssigneeID,
			Status:     byte(t.Status),
			StartDate:  t.StartDate,
			EndDate:    t.EndDate,
			Reminder: t.Reminder,
		}
	}
	return list, nil
}

func (h *taskUsecase) GetByID(ctx context.Context, id string) (Task, error) {
	task, err := h.taskRepo.GetByID(ctx, id)
	if err != nil {
		return Task{}, err
	}

	data := Task{
		ID:         task.ID,
		Title:      task.Title,
		Content:    task.Content,
		AssigneeID: task.AssigneeID,
		Status:     byte(task.Status),
		StartDate:  task.StartDate,
		EndDate:    task.EndDate,
		Reminder: task.Reminder,
	}

	return data, nil
}

func (h *taskUsecase) Store(ctx context.Context, t *Task) (string,error) {
	//TODO : add logic here
	task := &db.Task{
		Title:      t.Title,
		Content:    t.Content,
		AssigneeID: t.AssigneeID,
		Status:     db.Todo,
		StartDate:  t.StartDate,
		EndDate:    t.EndDate,
		Reminder: t.Reminder,
	}
	overlap, err := h.taskRepo.Overlap(ctx, task.AssigneeID, task.StartDate, task.EndDate)
	if err != nil {
		return "",err
	}

	if overlap {
		return "",errors.New("there is overlap")
	}

	err = h.taskRepo.Store(ctx, task)
	if err != nil {
		return "",err
	}

	// add reminder
	reminderID,err:=h.reminder.AddReminder(task.Reminder)
	if err!=nil {
		return "",err
	}
	err= h.reminderRepo.Store(ctx,&db.Reminder{
		ID:     reminderID,
		TaskID: task.ID,
	})

	if err!=nil {
		return "",err
	}

	return task.ID,nil
}

func (h *taskUsecase) UpdateStatus(ctx context.Context, task *UpdateTaskStatusRequest) error {
	_, err := h.taskRepo.GetByID(ctx, task.ID)
	if err != nil {
		return err
	}
	t := &db.Task{
		Model: db.Model{ID: task.ID},
	}
	if task.Status==0 {
		t.Status=db.Todo
	} else {
		t.Status=db.Done
	}

	err = h.taskRepo.UpdateStatus(ctx, t)
	if err != nil {
		return err
	}

	return nil
}

func (h *taskUsecase) Delete(ctx context.Context, id string) error {
	err := h.taskRepo.Delete(ctx, id)
	if err != nil {
		return err
	}
	reminder,err:=h.reminderRepo.GetByTaskID(ctx,id)
	if err!=nil {
		return err
	}
	err=h.reminder.RemoveReminder(reminder.ID)
	if err != nil {
		return err
	}
	return h.reminderRepo.Delete(ctx,id)
}
