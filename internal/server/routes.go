package server

import "github.com/labstack/echo/v4"

func RegisterRoutes(gr *echo.Group, user *UserHandler, task *TaskHandler) {
	gr.GET("/users", user.fetch)
	gr.GET("/users/:id", user.getByID)
	gr.DELETE("/users/:id", user.delete)
	gr.POST("/users", user.store)

	gr.GET("/tasks", task.fetch)
	gr.GET("/tasks/:id", task.getByID)
	gr.DELETE("/tasks/:id", task.delete)
	gr.POST("/tasks", task.store)
	gr.PUT("/tasks/:id", task.updateStatus)
}
