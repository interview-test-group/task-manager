package server

import (
	"context"
	"fmt"
	"gitlab.com/interview-test-group/task-manager/internal/config"
	"log"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"go.uber.org/fx"
)

// NewServer runs http server and listens to given port
func NewServer(lc fx.Lifecycle, conf *config.Config) *echo.Echo {
	e := echo.New()

	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: "[${time}] ${method} ${host}${uri} status=${status}\n",
	}))

	lc.Append(fx.Hook{
		OnStart: func(context.Context) error {
			log.Print(fmt.Sprintf("Listening at port %s...", conf.AppConfig.Port))
			go e.Start(fmt.Sprintf(":%s", conf.AppConfig.Port))
			return nil
		},
		OnStop: func(ctx context.Context) error {
			log.Print("Server is shutting down")
			return e.Shutdown(ctx)
		},
	})

	return e
}
