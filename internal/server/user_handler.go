package server

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/interview-test-group/task-manager/internal/validator"
	"net/http"
	"strings"
)

type UserHandler struct {
	validate *validator.Validator
	userUsecase UserUsecase
}

func NewUserHandler(validate *validator.Validator, user UserUsecase) *UserHandler {
	return &UserHandler{
		validate: validate,
		userUsecase: user,
	}
}

func (h *UserHandler) fetch(ctx echo.Context) error {
	users, err := h.userUsecase.Fetch(ctx.Request().Context())
	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, NewCustomInternalServerError(err.Error()))
	}

	return ctx.JSON(http.StatusOK, users)
}

func (h *UserHandler) getByID(ctx echo.Context) error {
	id := ctx.Param("id")
	user, err := h.userUsecase.GetByID(ctx.Request().Context(), id)
	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, NewCustomInternalServerError(err.Error()))
	}

	return ctx.JSON(http.StatusOK, user)
}

func (h *UserHandler) store(ctx echo.Context) error {
	user := User{}
	if err := ctx.Bind(&user); err != nil {
		return ctx.JSON(http.StatusBadRequest, NewCustomBadRequestError(err.Error()))
	}

	err := h.validate.Validate.Struct(&user)
	if err != nil {
		errList := h.validate.TranslateError(err, h.validate.Trans)
		return ctx.JSON(http.StatusBadRequest, NewCustomBadRequestError(strings.Join(errList, ", ")))
	}

	id,err := h.userUsecase.Store(ctx.Request().Context(), &user)
	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, NewCustomInternalServerError(err.Error()))
	}

	return ctx.JSON(http.StatusCreated, struct {
		ID string  `json:"id"`
	}{ID: id})
}

func (h *UserHandler) delete(ctx echo.Context) error {
	id := ctx.Param("id")
	err := h.userUsecase.Delete(ctx.Request().Context(), id)
	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, NewCustomInternalServerError(err.Error()))
	}

	return ctx.JSON(http.StatusOK, "User successfully deleted")
}
