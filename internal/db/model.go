package db

import (
	"time"

	uuid "github.com/satori/go.uuid"
	"gorm.io/gorm"

	_ "gorm.io/driver/postgres"
)

type Model struct {
	ID        string `gorm:"primary_key;"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt gorm.DeletedAt
}

type User struct {
	Model
	Firstname string
	Lastname  string
}

type Reminder struct {
	ID int
	TaskID string
}

type Status byte

const (
	Todo Status = iota
	Done
)

type Task struct {
	Model
	Title      string
	Content    string
	AssigneeID string
	Status     Status
	StartDate  time.Time
	EndDate    time.Time
	Reminder   time.Time
}

// BeforeCreate will set a UUID rather than numeric ID.
func (base *Model) BeforeCreate(scope *gorm.DB) error {
	base.ID = uuid.NewV4().String()
	return nil
}
