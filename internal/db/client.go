package db

import (
	"context"
	"fmt"
	"gitlab.com/interview-test-group/task-manager/internal/config"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"log"

	"go.uber.org/fx"
)

func newClient(conf *config.Config) (*gorm.DB, error) {
	connectionStr := fmt.Sprintf(
		"host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		conf.DBConfig.Host,
		conf.DBConfig.Port,
		conf.DBConfig.User,
		conf.DBConfig.Pass,
		conf.DBConfig.Name,
	)
	client, err := gorm.Open(postgres.Open(connectionStr), &gorm.Config{})

	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	return client, nil
}

// Migrate database schemas
func migrate(client *gorm.DB) error {
	return client.AutoMigrate(User{}, Task{},Reminder{})
}

func NewConnection(lc fx.Lifecycle, conf *config.Config) (*gorm.DB, error) {
	client, err := newClient(conf)
	if err != nil {
		return nil, err
	}

	lc.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			err := migrate(client)
			if err != nil {
				return err
			}
			return nil
		},
	})
	return client.Debug(), nil
}
