package db

import (
	"context"
	"gorm.io/gorm"
)

type UserRepository interface {
	Fetch(ctx context.Context) (res []User, err error)
	GetByID(ctx context.Context, id string) (User, error)
	Update(ctx context.Context, ar *User) error
	Store(ctx context.Context, a *User) error
	Delete(ctx context.Context, id string) error
}

type PostgresUserRepository struct {
	client *gorm.DB
}

func NewPostgresUserRepository(client *gorm.DB) UserRepository {
	return &PostgresUserRepository{
		client: client,
	}
}

func (db *PostgresUserRepository) GetByID(ctx context.Context, id string) (User, error) {
	user := User{}
	if err := db.client.WithContext(ctx).Where("id=?", id).First(&user).Error; err != nil {
		return user, err
	}
	return user, nil
}

func (db *PostgresUserRepository) Fetch(ctx context.Context) ([]User, error) {
	var users []User
	if err := db.client.WithContext(ctx).Find(&users).Error; err != nil {
		return nil, err
	}
	return users, nil
}

func (db *PostgresUserRepository) Store(ctx context.Context, user *User) error {
	return db.client.WithContext(ctx).Create(&user).Error
}

func (db *PostgresUserRepository) Update(ctx context.Context, agent *User) error {
	return db.client.WithContext(ctx).Model(&User{}).Updates(agent).Error
}

func (db *PostgresUserRepository) Delete(ctx context.Context, id string) error {
	if err := db.client.WithContext(ctx).Where("id = ?", id).Delete(&User{}).Error; err != nil {
		return err
	}
	return nil
}
