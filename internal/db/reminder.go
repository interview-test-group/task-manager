package db

import (
	"context"
	"gorm.io/gorm"
)

type ReminderRepository interface {
	GetByTaskID(ctx context.Context, id string) (Reminder,error)
	Store(ctx context.Context, reminder *Reminder) error
	Delete(ctx context.Context, id string) error
}

type PostgresTaskReminderRepository struct {
	client *gorm.DB
}

func NewReminderRepository(client *gorm.DB) ReminderRepository{
	return &PostgresTaskReminderRepository{client: client}
}
func (db *PostgresTaskReminderRepository) GetByTaskID(ctx context.Context, id string) (Reminder,error) {
	reminder:=Reminder{}
	err:=db.client.WithContext(ctx).Where("task_id=?",id).First(&reminder).Error
	if err!=nil{
		return Reminder{}, err
	}
	return reminder,nil
}

func (db *PostgresTaskReminderRepository) Store(ctx context.Context, reminder *Reminder) error {
	return db.client.WithContext(ctx).Create(&reminder).Error
}

func (db *PostgresTaskReminderRepository) Delete(ctx context.Context, id string) error {
	if err := db.client.WithContext(ctx).Where("task_id = ?", id).Delete(&Reminder{}).Error; err != nil {
		return err
	}
	return nil
}
