package db

import (
	"context"
	"gorm.io/gorm"
	"time"
)



type TaskRepository interface {
	Fetch(ctx context.Context) (res []Task, err error)
	GetByID(ctx context.Context, id string) (Task, error)
	UpdateStatus(ctx context.Context, ar *Task) error
	Store(ctx context.Context, a *Task) error
	Delete(ctx context.Context, id string) error
	Overlap(ctx context.Context, userID string, start time.Time, end time.Time) (bool, error)
}

type PostgresTaskRepository struct {
	client *gorm.DB
}

func NewPostgresTaskRepository(client *gorm.DB) TaskRepository {
	return &PostgresTaskRepository{
		client: client,
	}
}

func (db *PostgresTaskRepository) GetByID(ctx context.Context, id string) (Task, error) {
	task := Task{}
	if err := db.client.WithContext(ctx).Where("id=?", id).First(&task).Error; err != nil {
		return task, err
	}
	return task, nil
}

func (db *PostgresTaskRepository) Fetch(ctx context.Context) ([]Task, error) {
	var tasks []Task
	if err := db.client.WithContext(ctx).Find(&tasks).Error; err != nil {
		return nil, err
	}
	return tasks, nil
}

func (db *PostgresTaskRepository) Store(ctx context.Context, task *Task) error {
	return db.client.WithContext(ctx).Create(&task).Error
}

func (db *PostgresTaskRepository) UpdateStatus(ctx context.Context, task *Task) error {
	model:=Task{
		Model: Model{ID: task.ID},
	}
	return db.client.WithContext(ctx).Model(&model).Update("status",task.Status).Error
}

func (db *PostgresTaskRepository) Delete(ctx context.Context, id string) error {
	if err := db.client.WithContext(ctx).Where("id = ?", id).Delete(&Task{}).Error; err != nil {
		return err
	}
	return nil
}

func (db *PostgresTaskRepository) Overlap(ctx context.Context, userID string, start, end time.Time) (bool, error) {
	var tasks []Task
	if err := db.client.WithContext(ctx).Where("assignee_id=? and (start_date < ?) and (end_date > ?)", userID, end, start).Find(&tasks).Error; err != nil {
		return true, err
	}
	if len(tasks) > 0 {
		return true, nil
	}
	return false, nil
}
