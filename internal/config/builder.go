package config

import (
	"github.com/spf13/viper"
	"strings"
)

//dbConfig holds db settings
type dbConfig struct {
	Host    string
	Port    string
	User    string
	Pass    string
	Name    string
	SSLMode string
}

// AppConfig (host, port etc.)
type appConfig struct {
	Port string
}

// Config is the main struct for config collections and storage
type Config struct {
	DBConfig  dbConfig
	AppConfig appConfig
}

// NewConfig constructs configuration as a fx provider
func NewConfig() *Config {
	conf := viper.New()
	conf.AutomaticEnv()
	conf.SetEnvPrefix("tm")
	conf.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	return &Config{
		DBConfig: dbConfig{
			Host: conf.GetString("db.host"),
			Port: conf.GetString("db.port"),
			User: conf.GetString("db.user"),
			Pass: conf.GetString("db.pass"),
			Name: conf.GetString("db.name"),
		},
		AppConfig: appConfig{
			Port: conf.GetString("app.port"),
		},
	}

}
