package notification

type Notifier interface {
	 Notify(content string)
}

type MyNotifier struct {
	mailer Mailer
	pusher Pusher
}

func NewNotifier(mailer Mailer, pusher Pusher) Notifier {
	return &MyNotifier{
		mailer: mailer,
		pusher: pusher,
	}
}

func (n *MyNotifier) Notify(content string) {
	n.mailer.Send(content)
	n.pusher.Push(content)
}
