package notification

type Mailer interface {
	 Send(content string) error
}

type MyMailer struct {

}

func NewMailer() Mailer {
	return &MyMailer{}
}

func (m *MyMailer) Send(content string) error {
	println("mailer: ", content)
	return nil
}
