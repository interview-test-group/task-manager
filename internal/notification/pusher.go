package notification

type Pusher interface {
	Push(content string) error
}

type MyPusher struct {

}

func NewPusher() Pusher {
	return &MyPusher{}
}

func (p *MyPusher) Push(content string) error {
	// sample push notification implementation
	println("pusher", content)
	return nil
}