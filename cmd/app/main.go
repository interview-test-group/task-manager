package main

import (
	"gitlab.com/interview-test-group/task-manager/internal/config"
	"gitlab.com/interview-test-group/task-manager/internal/db"
	"gitlab.com/interview-test-group/task-manager/internal/server"
	"gitlab.com/interview-test-group/task-manager/internal/notification"
	"gitlab.com/interview-test-group/task-manager/internal/validator"
	"go.uber.org/fx"
)

func main() {

	app := fx.New(
		fx.Provide(
			config.NewConfig,
			db.NewConnection,
			db.NewPostgresUserRepository,
			db.NewPostgresTaskRepository,
			db.NewReminderRepository,

			validator.New,

			server.NewServer,
			server.NewUserHandler,
			server.NewTaskHandler,
			server.NewUserUsecase,
			server.NewTaskUsecase,
			server.NewReminder,

			notification.NewMailer,
			notification.NewPusher,
			notification.NewNotifier,
		),
		fx.Invoke(
			server.RegisterAllRoutes,
		),
	)
	app.Run()
}
